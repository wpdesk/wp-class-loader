[![pipeline status](https://gitlab.com/wpdesk/wp-class-loader/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-class-loader/commits/master) 
Integration: [![coverage report](https://gitlab.com/wpdesk/wp-class-loader/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-class-loader/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/wp-class-loader/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-class-loader/commits/master)

wp-class-loader
====================
