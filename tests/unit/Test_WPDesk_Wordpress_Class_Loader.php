<?php

class Test_WPDesk_Wordpress_Class_Loader extends PHPUnit_Framework_TestCase
{
    const NEWEST_PLUGIN_DATE = '2019-01-01';
    const OLD_PLUGIN_DATE = '2015-01-01';

    const DISABLED_PLUGIN_NAME = 'disabled';
    const DEFAULT_CLASS_ALIAS = 'Class';
    const NOT_EXISTENT_CLASS_NAME = 'ClassNoExistent';

    public function setUp()
    {
        \WP_Mock::setUp();

        \WP_Mock::userFunction('is_plugin_active', [
            'return' => function ($name) {
                return $name !== self::DISABLED_PLUGIN_NAME;
            }
        ]);
    }

    public function tearDown()
    {
        \WP_Mock::tearDown();

        global $wpdesk_wordpress_class_loader_registry;
        $wpdesk_wordpress_class_loader_registry = null;
    }

    public function test_can_create_instance()
    {
        $loader = $this->prepare_new_plugin_loader();

        $this->register_stub_class($loader, ClassA::class, self::DEFAULT_CLASS_ALIAS);
        $class_instance = $loader->create_instance(self::DEFAULT_CLASS_ALIAS);
        $this->assertInstanceOf(ClassA::class, $class_instance, 'Cant even make instance of a class');

        $this->register_stub_class($loader, ClassWithConstructor::class, 'ClassWithConstructor');
        $class_instance = $loader->create_instance('ClassWithConstructor', ['some_args']);
        $this->assertInstanceOf(ClassWithConstructor::class, $class_instance,
            'Object with constructor parameter should be returned');
    }

    /**
     * @return WPDesk_Wordpress_Class_Loader
     */
    private function prepare_new_plugin_loader()
    {
        return new WPDesk_Wordpress_Class_Loader('new_plugin', self::NEWEST_PLUGIN_DATE);
    }

    /**
     * Registers stub class from /Stub dir to given loader
     *
     * @param WPDesk_Wordpress_Class_Loader $loader
     * @param string $class to pass to register method
     * @param string $interface to pass to register method
     */
    private function register_stub_class(WPDesk_Wordpress_Class_Loader $loader, $class, $interface)
    {
        $loader->register_class(__DIR__ . '/Stub/' . $class . '.php', $class, $interface);
    }

    public function test_can_get_fallback_class_when_first_disabled()
    {
        $loader_disabled = $this->prepare_disabled_plugin_loader();
        $this->register_stub_class($loader_disabled, ClassA::class, self::DEFAULT_CLASS_ALIAS);

        $loader_old = $this->prepare_old_plugin_loader();
        $this->register_stub_class($loader_old, ClassB::class, self::DEFAULT_CLASS_ALIAS);

        $class_instance = $loader_old->create_instance(self::DEFAULT_CLASS_ALIAS);
        $this->assertInstanceOf(ClassB::class, $class_instance, 'Older class should be returned as newest is disabled');
    }

    /**
     * @return WPDesk_Wordpress_Class_Loader
     */
    private function prepare_disabled_plugin_loader()
    {
        return new WPDesk_Wordpress_Class_Loader(self::DISABLED_PLUGIN_NAME, self::NEWEST_PLUGIN_DATE);
    }

    /**
     * @return WPDesk_Wordpress_Class_Loader
     */
    private function prepare_old_plugin_loader()
    {
        return new WPDesk_Wordpress_Class_Loader('old_plugin', self::OLD_PLUGIN_DATE);
    }

    public function test_can_get_fallback_class_when_first_not_found()
    {
        $loader_new = $this->prepare_new_plugin_loader();
        $loader_new->register_class(__DIR__ . '/Stub/NotExistentPath', ClassA::class, self::DEFAULT_CLASS_ALIAS);

        $loader_old = $this->prepare_old_plugin_loader();
        $this->register_stub_class($loader_old, ClassB::class, self::DEFAULT_CLASS_ALIAS);

        $class_instance = $loader_old->create_instance(self::DEFAULT_CLASS_ALIAS);
        $this->assertInstanceOf(ClassB::class, $class_instance,
            'Older class should be returned as newest cant be found');
    }

    public function test_exception_thrown_when_all_disabled_and_not_using_disabled_loader()
    {
        $loader_disabled = $this->prepare_disabled_plugin_loader();
        $this->register_stub_class($loader_disabled, ClassB::class, self::DEFAULT_CLASS_ALIAS);

        $this->expectException(RuntimeException::class);
        $not_disabled_loader = $this->prepare_old_plugin_loader();
        $not_disabled_loader->create_instance(self::DEFAULT_CLASS_ALIAS);
    }

    public function test_can_use_disabled_plugin_when_using_disabled_loader()
    {
        $loader_disabled = $this->prepare_disabled_plugin_loader();
        $this->register_stub_class($loader_disabled, ClassB::class, self::DEFAULT_CLASS_ALIAS);

        $class_instance = $loader_disabled->create_instance(self::DEFAULT_CLASS_ALIAS);
        $this->assertInstanceOf(ClassB::class, $class_instance,
            'Instantiation for disabled plugin when ising this plugin loader loader should work as this code can be executed during enabling it');
    }

    public function test_can_find_newer_class_when_loaded_first()
    {
        $loader_new = $this->prepare_new_plugin_loader();
        $this->register_stub_class($loader_new, ClassA::class, self::DEFAULT_CLASS_ALIAS);

        $loader_old = $this->prepare_old_plugin_loader();
        $this->register_stub_class($loader_old, ClassB::class, self::DEFAULT_CLASS_ALIAS);


        $class_instance = $loader_old->create_instance(self::DEFAULT_CLASS_ALIAS);
        $this->assertInstanceOf(ClassA::class, $class_instance,
            'Newest class should be returned regardless of used loader');
        $class_instance = $loader_new->create_instance(self::DEFAULT_CLASS_ALIAS);
        $this->assertInstanceOf(ClassA::class, $class_instance,
            'Newest class should be returned regardless of used loader');
    }

    public function test_can_find_newer_class_when_loaded_last()
    {
        $loader_old = $this->prepare_old_plugin_loader();
        $this->register_stub_class($loader_old, ClassB::class, self::DEFAULT_CLASS_ALIAS);

        $loader_new = $this->prepare_new_plugin_loader();
        $this->register_stub_class($loader_new, ClassA::class, self::DEFAULT_CLASS_ALIAS);

        $class_instance = $loader_old->create_instance(self::DEFAULT_CLASS_ALIAS);
        $this->assertInstanceOf(ClassA::class, $class_instance,
            'Newest class should be returned regardless of used loader');
        $class_instance = $loader_new->create_instance(self::DEFAULT_CLASS_ALIAS);
        $this->assertInstanceOf(ClassA::class, $class_instance,
            'Newest class should be returned regardless of used loader');
    }

    public function test_can_find_newer_class_when_many_random_appeared()
    {
        $this->load_random_classes_type_b(100);

        $loader_new = $this->prepare_new_plugin_loader();
        $this->register_stub_class($loader_new, ClassA::class, self::DEFAULT_CLASS_ALIAS);

        $this->load_random_classes_type_b(100);

        $random_loader  = $this->prepare_random_plugin_loader(self::NEWEST_PLUGIN_DATE);
        $class_instance = $random_loader->create_instance(self::DEFAULT_CLASS_ALIAS);
        $this->assertInstanceOf(ClassA::class, $class_instance,
            'ClassA should be returned as it\'s created with newest plugin');
    }

    /**
     * @param int $how_many
     */
    private function load_random_classes_type_b($how_many)
    {
        for ($i = 0; $i < $how_many; $i++) {
            $random_loader = $this->prepare_random_plugin_loader(self::NEWEST_PLUGIN_DATE);
            $this->register_stub_class($random_loader, ClassB::class, self::DEFAULT_CLASS_ALIAS);
        }
    }

    /**
     * @return WPDesk_Wordpress_Class_Loader
     */
    private function prepare_random_plugin_loader($older_than)
    {
        $week        = 7 * 24 * 60 * 60;
        $random_date = mt_rand(1, strtotime($older_than) - $week);

        return new WPDesk_Wordpress_Class_Loader(uniqid('some_random_name', true), date('Y-m-d', $random_date));
    }

    public function test_can_load_more_than_once_class()
    {
        $loaderB = $this->prepare_old_plugin_loader();
        $this->register_stub_class($loaderB, ClassB::class, ClassB::class);

        $loaderA = $this->prepare_new_plugin_loader();
        $this->register_stub_class($loaderA, ClassA::class, ClassA::class);

        $class_instance = $loaderA->create_instance(ClassB::class);
        $this->assertInstanceOf(ClassB::class, $class_instance, 'Asked explicitly for class');
        $class_instance = $loaderB->create_instance(ClassA::class);
        $this->assertInstanceOf(ClassA::class, $class_instance, 'Asked explicitly for class');
    }

    public function test_exception_thrown_with_invalid_class_name()
    {
        $loader = $this->prepare_new_plugin_loader();

        $loader->register_class(__DIR__ . '/Stub/ClassA.php', self::NOT_EXISTENT_CLASS_NAME, self::DEFAULT_CLASS_ALIAS);
        $this->expectException(RuntimeException::class);
        $loader->create_instance(self::NOT_EXISTENT_CLASS_NAME);
    }

    public function test_exception_thrown_with_invalid_constructor_params()
    {
        $loader = $this->prepare_new_plugin_loader();

        $loader->register_class(__DIR__ . '/Stub/ClassA.php', self::NOT_EXISTENT_CLASS_NAME, self::DEFAULT_CLASS_ALIAS);
        $this->expectException(ReflectionException::class);
        $loader->create_instance(self::DEFAULT_CLASS_ALIAS, ['some invalid params']);
    }

    public function test_exception_thrown_with_invalid_dir_name()
    {
        $loader = $this->prepare_new_plugin_loader();

        $loader->register_class('dir that not exist', ClassA::class, self::DEFAULT_CLASS_ALIAS);
        $this->expectException(RuntimeException::class);
        $loader->create_instance(self::DEFAULT_CLASS_ALIAS);
    }

    public function test_exception_thrown_no_register()
    {
        $loader = $this->prepare_new_plugin_loader();

        $this->expectException(RuntimeException::class);
        $loader->create_instance('whatever');
    }

    public function test_exception_thrown_with_class_name_mismatch()
    {
        $loader = $this->prepare_new_plugin_loader();

        $loader->register_class(__DIR__ . '/Stub/ClassA.php', self::NOT_EXISTENT_CLASS_NAME, self::DEFAULT_CLASS_ALIAS);
        $this->expectException(ReflectionException::class);
        $loader->create_instance(self::DEFAULT_CLASS_ALIAS);
    }
}